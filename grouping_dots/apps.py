from django.apps import AppConfig


class GroupingDotsConfig(AppConfig):
    name = 'grouping_dots'
