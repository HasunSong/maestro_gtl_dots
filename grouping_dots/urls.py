from django.urls import path, re_path#정규 표현식 이용해서 복잡한 url 표현할 때 유용, 알아두자.

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('get_game_data/', views.get_game_data, name="get_game_data"),
    path('test_data/', views.test_data, name="test_data")
]

